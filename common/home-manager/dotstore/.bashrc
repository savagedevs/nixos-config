#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#Command Time Functions
roundseconds (){
  # rounds a number to 3 decimal places
  echo m=$1";h=0.5;scale=4;t=1000;if(m<0) h=-0.5;a=m*t+h;scale=3;a/t;" | bc
}

bash_getstarttime (){
  # places the epoch time in ns into shared memory
  date +%s.%N >"/dev/shm/${USER}.bashtime.${1}"
}

bash_getstoptime (){
  # reads stored epoch time and subtracts from current
  local endtime=$(date +%s.%N)
  local starttime=$(cat /dev/shm/${USER}.bashtime.${1})
  roundseconds $(echo $(eval echo "$endtime - $starttime") | bc)
}

# Define up function
up () {
  local d=""
  local limit="$1"

  # Default to limit of 1
  if [ -z "$limit" ] || [ "$limit" -le 0 ]; then
    limit=1
  fi

  for ((i=1;i<=limit;i++)); do
    d="../$d"
  done

  # perform cd. Show error if cd fails
  if ! cd "$d"; then
    echo "Couldn't go up $limit dirs.";
  fi
}

#Define git function
get_git_status() {
  # Check if the current directory is in a git repository
  if git rev-parse --git-dir > /dev/null 2>&1; then
    # Check the status of the repository
    local status=$(git status --porcelain)
    local ahead=$(git rev-list @{u}..HEAD 2>/dev/null)
    local behind=$(git rev-list HEAD..@{u} 2>/dev/null)
    
    if [[ -z $status ]]; then
      if [[ -n $ahead && -n $behind ]]; then
        echo "| ⇕"
      elif [[ -n $ahead ]]; then
        echo "| ↑"
      elif [[ -n $behind ]]; then
        echo "| ↓"
      else
        echo "| ✔"
      fi
    else
      if [[ -n $ahead ]]; then
        echo "| ↑*"
      elif [[ -n $behind ]]; then
        echo "| ↓*"
      else
        echo "| *"
      fi
    fi
  fi
}

ROOTPID=$BASHPID
bash_getstarttime $ROOTPID

PS0='$(bash_getstarttime $ROOTPID)'
PS1='Last Command Took $(bash_getstoptime $ROOTPID)s $(get_git_status)\n'
PS1="$PS1"'[\u] \w > '

export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export QT_QPA_PLATFORMTHEME=qt5ct
export TERMINAL=alacritty
export PROTON_HIDE_NVIDIA_GPU=0
export PROTON_ENABLE_NVAPI=1
export VKD3D_CONFIG=dxr,dxr11
export PROTON_ENABLE_NGX_UPDATER=1
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"

neofetch

alias ls='ls -lah --color=auto'
alias mkdir='mkdir -pv'

alias mv='mv -i'
alias cp='cp -i'
alias rm='rm -i'
alias ln='ln -i'
alias clear='clear && neofetch'
