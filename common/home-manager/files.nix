{ config, pkgs, inputs, ... }:

{
  home-manager.users.daniel.home.file = {
    ".bashrc".source = dotstore/.bashrc;
    ".config/alacritty/alacritty.yml".source = dotstore/.config/alacritty/alacritty.yml;
    ".config/btop/btop.conf".source = dotstore/.config/btop/btop.conf;
    ".config/btop/themes/rose-pine.theme".source = dotstore/.config/btop/themes/rose-pine.theme;
    ".config/neofetch/config.conf".source = dotstore/.config/neofetch/config.conf;
    ".config/onlyoffice/DesktopEditors.conf".source = dotstore/.config/onlyoffice/DesktopEditors.conf;
  };
}
