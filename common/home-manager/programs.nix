{ config, pkgs, inputs, ... }:
let
  system = "x86_64-linux";
in
{
  home-manager.users.daniel.programs = {
    vscode = {
      enable = true;
      package = pkgs.vscodium;
      extensions =
        with inputs.nix-vscode-extensions.extensions.${system}.vscode-marketplace;
        with inputs.nix-vscode-extensions.extensions.${system}.open-vsx;
        with inputs.nix-vscode-extensions.extensions.${system}.open-vsx-release;
      [
        jnoortheen.nix-ide
        arrterian.nix-env-selector
        mvllow.rose-pine
        rust-lang.rust-analyzer
        serayuzgur.crates
        usernamehw.errorlens
        tamasfe.even-better-toml
        gitlab.gitlab-workflow
        jscearcy.rust-doc-viewer
      ];
    };

    git = {
      enable = true;
      userName = "Daniel McLarty";
      userEmail = "daniel@savagedevs.com";
    };

    home-manager.enable = true;
  };
}

