{ config, lib, pkgs, inputs, ... }:

{
  users.users.daniel = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "openrazer" ];
    packages = with pkgs; [
      # User Apps
      firefox
      thunderbird
      qbittorrent
      onlyoffice-bin
      jellyfin-media-player
      zoom-us
      vlc

      # Game apps
      discord
      lutris
      prismlauncher
      obs-studio

      # Utiity Apps
      alacritty
      wine-staging
      sidequest
      nextcloud-client
      protonup-qt

      # Theming/control
      betterdiscord-installer
      polychromatic
      rose-pine-gtk-theme
      rose-pine-icon-theme
      vistafonts
      via
    ];
  };
}
