{ config, lib, pkgs, inputs, ... }:

{
  services = {
    xserver = {
      enable = true;
      xkb.layout = "us";
    };

    displayManager.sddm.enable = true;
    desktopManager.plasma6.enable = true;

    printing.enable = true;

    libinput.enable = true;

    openssh.enable = true;

    mullvad-vpn.enable = true;
  };
}
