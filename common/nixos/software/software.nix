{ config, lib, pkgs, inputs, ... }:

{
  environment.systemPackages = with pkgs; [
    # Background apps/daemons
    os-prober
    plymouth
    mullvad-vpn
    openrazer-daemon
    linuxPackages.v4l2loopback
    xwaylandvideobridge
    glib
    gsettings-desktop-schemas

    # Terminal Apps
    btop
    curl
    bc
    neofetch
    wget
    git

    # Misc
    inputs.kwin-effects-forceblur.packages.${pkgs.system}.default
  ];

  # Add the nerd fonts.
  fonts.packages = with pkgs; [
    (nerdfonts.override { fonts = [ "JetBrainsMono" ]; })
  ];

  programs = {
    mtr.enable = true;

    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };

    steam = {
      enable = true;
      remotePlay.openFirewall = true;
      dedicatedServer.openFirewall = true;
      gamescopeSession.enable = true;
    };
  };
}
