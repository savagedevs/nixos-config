{ config, lib, pkgs, inputs, ... }:

{
  imports = [ 
    # Include the results of the hardware scan.
    ./hardware-configuration.nix

    # Include hardware options.
    ../../common/nixos/hardware/boot.nix
    ../../common/nixos/hardware/other.nix
    ../../common/nixos/hardware/sound.nix
    custom/hardware.nix

    # Include software options.
    ../../common/nixos/software/services.nix
    ../../common/nixos/software/software.nix

    # Include everything else.
    ../../common/nixos/other/allow.nix
    ../../common/nixos/other/user.nix

    # Include home manager modules.
    ../../common/home-manager/programs.nix
    ../../common/home-manager/files.nix

    inputs.home-manager.nixosModules.default
  ];

  home-manager = {
    extraSpecialArgs = { inherit inputs; };
    users = {
      "daniel" = import ./home.nix;
    };
  };

  networking.hostName = "homepc";

  # NEVER change this EVER!!!
  system.stateVersion = "24.05"; # Did you read the comment?
}
