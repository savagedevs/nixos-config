{ config, lib, pkgs, inputs, ... }:

{
  hardware = {
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };

    nvidia = {
      modesetting.enable = true;
      powerManagement.enable = false;
      powerManagement.finegrained = false;
      open = true;
      nvidiaSettings = true;

      package = config.boot.kernelPackages.nvidiaPackages.stable;
    };
  };

  boot.kernelParams = [ 
    "nvidia-drm.modeset=1" 
    "nvidia-drm.fbdev=1" 
    "NVreg_EnableGpuFirmware=0" 
    "module_blacklist=amdgpu" 
  ];

  services.xserver.videoDrivers = [ "nvidia" ];
} 
