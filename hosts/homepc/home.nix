{ inputs, config, pkgs, ... }:

{
  home.username = "daniel";
  home.homeDirectory = "/home/daniel";

  # NEVER CHANGE THIS EVER!!!
  home.stateVersion = "24.05"; # Please read the comment before changing.
}
