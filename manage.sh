#!/usr/bin/env bash

# Absolute path to your configuration
CONFIG_PATH=~/.sysconfig

# Name of your NixOS configuration
CONFIG_NAME=homepc

# Change to the configuration directory
cd "$CONFIG_PATH" || { echo "Failed to change directory to $CONFIG_PATH"; exit 1; }

# Update the flake
nix flake update || { echo "Failed to update flake"; exit 1; }

# Commit changes to git with the current date and time
current_date_time=$(date +"%Y-%m-%d %H:%M:%S")
git add *
git commit -m "Updated system at $current_date_time" || { echo "Git commit failed"; exit 1; }
git push origin master

# Rebuild the system
sudo nixos-rebuild switch --flake "$CONFIG_PATH#$CONFIG_NAME" || { echo "NixOS rebuild failed"; exit 1; }

# Remove older generations, keeping only the most recent two
sudo nix-collect-garbage -d

echo "Flake updated, system rebuilt, and old builds purged successfully."
